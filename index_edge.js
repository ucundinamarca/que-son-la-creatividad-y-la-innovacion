/*jslint */
/*global AdobeEdge: false, window: false, document: false, console:false, alert: false */
(function (compId) {

    "use strict";
    var im='images/',
        aud='media/',
        vid='media/',
        js='js/',
        fonts = {
        },
        opts = {
            'gAudioPreloadPreference': 'auto',
            'gVideoPreloadPreference': 'auto'
        },
        resources = [
        ],
        scripts = [
        ],
        symbols = {
            "stage": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "both",
                centerStage: "both",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            id: 'stage1',
                            type: 'group',
                            rect: ['-165', '16', '1093', '707', 'auto', 'auto'],
                            c: [
                            {
                                id: 'r64',
                                type: 'image',
                                rect: ['437px', '479px', '162px', '115px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"r64.png",'0px','0px'],
                                userClass: "tuercas"
                            },
                            {
                                id: 'r67',
                                type: 'image',
                                rect: ['398px', '0px', '159px', '123px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"r67.png",'0px','0px'],
                                userClass: "tuercas"
                            },
                            {
                                id: 'r63',
                                type: 'image',
                                rect: ['-1px', '0px', '1022px', '707px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"r63.png",'0px','0px']
                            },
                            {
                                id: 'r66',
                                type: 'image',
                                rect: ['633px', '129px', '460px', '207px', 'auto', 'auto'],
                                title: 'Título: ¿qué son la creatividad y la innovación?',
                                tabindex: '1',
                                fill: ["rgba(0,0,0,0)",im+"r66.png",'0px','0px']
                            },
                            {
                                id: 'btn_siguiente',
                                type: 'image',
                                rect: ['633px', '341px', '113px', '113px', 'auto', 'auto'],
                                title: 'Siguiente',
                                tabindex: '2',
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"r65.png",'0px','0px']
                            }]
                        },
                        {
                            id: 'stage2',
                            type: 'group',
                            rect: ['0px', '16', '1136', '806', 'auto', 'auto'],
                            c: [
                            {
                                id: 'r69',
                                type: 'image',
                                rect: ['590px', '421px', '546px', '385px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"r69.png",'0px','0px']
                            },
                            {
                                id: 'r68',
                                type: 'image',
                                rect: ['0px', '10px', '393px', '51px', 'auto', 'auto'],
                                title: 'título: ¿qué son la creatividad y la innovación?',
                                tabindex: '3',
                                fill: ["rgba(0,0,0,0)",im+"r68.png",'0px','0px']
                            },
                            {
                                id: 'btn_bibliografia',
                                type: 'image',
                                rect: ['817px', '0px', '65px', '73px', 'auto', 'auto'],
                                title: 'botón bibliografía',
                                tabindex: '4',
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"r70.png",'0px','0px'],
                                userClass: "btn_options"
                            },
                            {
                                id: 'btn_creditos',
                                type: 'image',
                                rect: ['928px', '0px', '58px', '73px', 'auto', 'auto'],
                                title: 'botón créditos',
                                tabindex: '5',
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"r71.png",'0px','0px'],
                                userClass: "btn_options"
                            },
                            {
                                id: 'btn_creatividad',
                                type: 'image',
                                rect: ['172px', '179px', '277px', '291px', 'auto', 'auto'],
                                title: 'botón tema creatividad',
                                tabindex: '6',
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"r73.png",'0px','0px'],
                                userClass: "btn_temas"
                            },
                            {
                                id: 'btn_inovacion',
                                type: 'image',
                                rect: ['583px', '179px', '264px', '291px', 'auto', 'auto'],
                                title: 'botón tema inovación',
                                tabindex: '7',
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"r72.png",'0px','0px'],
                                userClass: "btn_temas"
                            }]
                        },
                        {
                            id: 'stage3',
                            type: 'group',
                            rect: ['0px', '16px', '1136', '806', 'auto', 'auto'],
                            c: [
                            {
                                id: 'r69Copy',
                                type: 'image',
                                rect: ['590px', '421px', '546px', '385px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"r69.png",'0px','0px']
                            },
                            {
                                id: 'r68Copy',
                                type: 'image',
                                rect: ['0px', '10px', '393px', '51px', 'auto', 'auto'],
                                title: 'título: ¿qué son la creatividad y la innovación?',
                                tabindex: '8',
                                fill: ["rgba(0,0,0,0)",im+"r68.png",'0px','0px']
                            },
                            {
                                id: 'slider',
                                type: 'rect',
                                rect: ['0px', '123px', '1022px', '413px', 'auto', 'auto'],
                                fill: ["rgba(255,255,255,0.00)"],
                                stroke: [0,"rgba(0,0,0,1)","none"],
                                c: [
                                {
                                    id: 'container-slider',
                                    type: 'rect',
                                    rect: ['0px', '0px', '1022px', '413px', 'auto', 'auto'],
                                    fill: ["rgba(255,255,255,0.00)"],
                                    stroke: [0,"rgba(0,0,0,1)","none"],
                                    c: [
                                    {
                                        id: 'slide1',
                                        type: 'rect',
                                        rect: ['0px', '0px', '1022px', '413px', 'auto', 'auto'],
                                        fill: ["rgba(255,255,255,0.04)"],
                                        stroke: [0,"rgba(0,0,0,1)","none"],
                                        userClass: "sliders",
                                        c: [
                                        {
                                            id: 'r77',
                                            type: 'image',
                                            rect: ['774px', '-64px', '423px', '554px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"r77.png",'0px','0px']
                                        },
                                        {
                                            id: 'slider_text_1',
                                            type: 'rect',
                                            rect: ['30px', '-46px', '735px', '439px', 'auto', 'auto'],
                                            fill: ["rgba(255,255,255,0.04)"],
                                            stroke: [0,"rgb(0, 0, 0)","none"],
                                            userClass: "containers"
                                        }]
                                    },
                                    {
                                        id: 'slide2',
                                        type: 'rect',
                                        rect: ['0px', '0px', '1022px', '413px', 'auto', 'auto'],
                                        fill: ["rgba(255,255,255,0.00)"],
                                        stroke: [0,"rgba(0,0,0,1)","none"],
                                        userClass: "sliders",
                                        c: [
                                        {
                                            id: 'r80',
                                            type: 'image',
                                            rect: ['748px', '-37px', '403px', '547px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"r80.png",'0px','0px']
                                        },
                                        {
                                            id: 'slider_text_2',
                                            type: 'rect',
                                            rect: ['30px', '-46px', '735px', '439px', 'auto', 'auto'],
                                            fill: ["rgba(255,255,255,0.00)"],
                                            stroke: [0,"rgb(0, 0, 0)","none"],
                                            userClass: "containers"
                                        }]
                                    },
                                    {
                                        id: 'slide3',
                                        type: 'rect',
                                        rect: ['0px', '0px', '1022px', '413px', 'auto', 'auto'],
                                        fill: ["rgba(255,255,255,0.00)"],
                                        stroke: [0,"rgba(0,0,0,1)","none"],
                                        userClass: "sliders",
                                        c: [
                                        {
                                            id: 'r85',
                                            type: 'image',
                                            rect: ['-100px', '-96px', '378px', '544px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"r85.png",'0px','0px']
                                        },
                                        {
                                            id: 'r88',
                                            type: 'image',
                                            rect: ['298px', '14px', '570px', '274px', 'auto', 'auto'],
                                            title: '¿El pensamiento divergente es un sinónimo de la creatividad?',
                                            tabindex: '1',
                                            fill: ["rgba(0,0,0,0)",im+"r88.png",'0px','0px']
                                        },
                                        {
                                            id: 'btn-f1',
                                            type: 'group',
                                            rect: ['283', '332', '262', '108', 'auto', 'auto'],
                                            title: 'Falso',
                                            tabindex: '2',
                                            cursor: 'pointer',
                                            userClass: "btn vf",
                                            c: [
                                            {
                                                id: 'btn-f1-1',
                                                type: 'image',
                                                rect: ['0px', '0px', '262px', '108px', 'auto', 'auto'],
                                                fill: ["rgba(0,0,0,0)",im+"r86.png",'0px','0px']
                                            },
                                            {
                                                id: 'btn-f1-2',
                                                type: 'image',
                                                rect: ['0px', '0px', '260px', '107px', 'auto', 'auto'],
                                                fill: ["rgba(0,0,0,0)",im+"r81.png",'0px','0px']
                                            },
                                            {
                                                id: 'btn-f1-3',
                                                type: 'image',
                                                rect: ['0px', '0px', '260px', '107px', 'auto', 'auto'],
                                                fill: ["rgba(0,0,0,0)",im+"r83.png",'0px','0px']
                                            }]
                                        },
                                        {
                                            id: 'btn-v1',
                                            type: 'group',
                                            rect: ['617', '332', '262', '108', 'auto', 'auto'],
                                            title: 'Verdadero',
                                            tabindex: '3',
                                            cursor: 'pointer',
                                            userClass: "btn vf",
                                            c: [
                                            {
                                                id: 'btn-v1-1',
                                                type: 'image',
                                                rect: ['0px', '0px', '262px', '108px', 'auto', 'auto'],
                                                fill: ["rgba(0,0,0,0)",im+"r87.png",'0px','0px']
                                            },
                                            {
                                                id: 'btn-v1-2',
                                                type: 'image',
                                                rect: ['0px', '0px', '260px', '107px', 'auto', 'auto'],
                                                fill: ["rgba(0,0,0,0)",im+"r82.png",'0px','0px']
                                            },
                                            {
                                                id: 'btn-v1-3',
                                                type: 'image',
                                                rect: ['0px', '0px', '260px', '107px', 'auto', 'auto'],
                                                fill: ["rgba(0,0,0,0)",im+"r84.png",'0px','0px']
                                            }]
                                        }]
                                    },
                                    {
                                        id: 'slide4',
                                        type: 'rect',
                                        rect: ['0px', '0px', '1022px', '413px', 'auto', 'auto'],
                                        fill: ["rgba(255,255,255,0.00)"],
                                        stroke: [0,"rgba(0,0,0,1)","none"],
                                        userClass: "sliders",
                                        c: [
                                        {
                                            id: 'slider_text_4',
                                            type: 'rect',
                                            rect: ['30px', '-46px', '948px', '439px', 'auto', 'auto'],
                                            fill: ["rgba(255,255,255,0.00)"],
                                            stroke: [0,"rgb(0, 0, 0)","none"],
                                            userClass: "containers"
                                        }]
                                    },
                                    {
                                        id: 'slide5',
                                        type: 'rect',
                                        rect: ['0px', '0px', '1022px', '413px', 'auto', 'auto'],
                                        fill: ["rgba(255,255,255,0.07)"],
                                        stroke: [0,"rgba(0,0,0,1)","none"],
                                        userClass: "sliders",
                                        c: [
                                        {
                                            id: 'slider_text_5',
                                            type: 'rect',
                                            rect: ['30px', '-46px', '948px', '439px', 'auto', 'auto'],
                                            fill: ["rgba(255,255,255,0.07)"],
                                            stroke: [0,"rgb(0, 0, 0)","none"],
                                            userClass: "containers"
                                        }]
                                    },
                                    {
                                        id: 'slide6',
                                        type: 'rect',
                                        rect: ['0px', '0px', '1022px', '413px', 'auto', 'auto'],
                                        fill: ["rgba(255,255,255,0.00)"],
                                        stroke: [0,"rgba(0,0,0,1)","none"],
                                        userClass: "sliders",
                                        c: [
                                        {
                                            id: 'slider_text_6',
                                            type: 'rect',
                                            rect: ['30px', '-46px', '948px', '439px', 'auto', 'auto'],
                                            fill: ["rgba(255,255,255,0.00)"],
                                            stroke: [0,"rgb(0, 0, 0)","none"],
                                            userClass: "containers"
                                        }]
                                    },
                                    {
                                        id: 'slide7',
                                        type: 'rect',
                                        rect: ['0px', '0px', '1022px', '413px', 'auto', 'auto'],
                                        fill: ["rgba(255,255,255,0.00)"],
                                        stroke: [0,"rgba(0,0,0,1)","none"],
                                        userClass: "sliders",
                                        c: [
                                        {
                                            id: 'slider_text_7',
                                            type: 'rect',
                                            rect: ['30px', '-46px', '948px', '439px', 'auto', 'auto'],
                                            fill: ["rgba(255,255,255,0.00)"],
                                            stroke: [0,"rgb(0, 0, 0)","none"],
                                            userClass: "containers"
                                        }]
                                    },
                                    {
                                        id: 'slide8',
                                        type: 'rect',
                                        rect: ['0px', '0px', '1022px', '413px', 'auto', 'auto'],
                                        fill: ["rgba(255,255,255,0.00)"],
                                        stroke: [0,"rgba(0,0,0,1)","none"],
                                        userClass: "sliders",
                                        c: [
                                        {
                                            id: 'r92',
                                            type: 'image',
                                            rect: ['149px', '52px', '751px', '274px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"r92.png",'0px','0px']
                                        },
                                        {
                                            id: 'r93',
                                            type: 'image',
                                            rect: ['797px', '255px', '144px', '153px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"r93.png",'0px','0px']
                                        },
                                        {
                                            id: 'r94',
                                            type: 'image',
                                            rect: ['74px', '-43px', '149px', '150px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"r94.png",'0px','0px']
                                        }]
                                    },
                                    {
                                        id: 'slide9',
                                        type: 'rect',
                                        rect: ['0px', '0px', '1022px', '413px', 'auto', 'auto'],
                                        fill: ["rgba(255,255,255,0.00)"],
                                        stroke: [0,"rgba(0,0,0,1)","none"],
                                        userClass: "sliders",
                                        c: [
                                        {
                                            id: 'icon_8',
                                            type: 'image',
                                            rect: ['640px', '98px', '419px', '398px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"icon_8.svg",'0px','0px']
                                        },
                                        {
                                            id: 'slider_text_8',
                                            type: 'rect',
                                            rect: ['30px', '-46px', '610px', '439px', 'auto', 'auto'],
                                            fill: ["rgba(255,255,255,0.00)"],
                                            stroke: [0,"rgb(0, 0, 0)","none"],
                                            userClass: "containers"
                                        }]
                                    },
                                    {
                                        id: 'slide10',
                                        type: 'rect',
                                        rect: ['0px', '0px', '1022px', '413px', 'auto', 'auto'],
                                        fill: ["rgba(255,255,255,0.00)"],
                                        stroke: [0,"rgba(0,0,0,1)","none"],
                                        userClass: "sliders",
                                        c: [
                                        {
                                            id: 'r96',
                                            type: 'image',
                                            rect: ['-84px', '-48px', '316px', '456px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"r96.png",'0px','0px']
                                        },
                                        {
                                            id: 'btn-f2',
                                            type: 'group',
                                            rect: ['283', '332', '262', '108', 'auto', 'auto'],
                                            title: 'Botón Falso',
                                            cursor: 'pointer',
                                            userClass: "btn",
                                            c: [
                                            {
                                                id: 'btn-f2-1',
                                                type: 'image',
                                                rect: ['0px', '0px', '262px', '108px', 'auto', 'auto'],
                                                fill: ["rgba(0,0,0,0)",im+"r86.png",'0px','0px']
                                            },
                                            {
                                                id: 'btn-f2-2',
                                                type: 'image',
                                                rect: ['0px', '0px', '260px', '107px', 'auto', 'auto'],
                                                fill: ["rgba(0,0,0,0)",im+"r81.png",'0px','0px']
                                            },
                                            {
                                                id: 'btn-f2-3',
                                                type: 'image',
                                                rect: ['0px', '0px', '260px', '107px', 'auto', 'auto'],
                                                fill: ["rgba(0,0,0,0)",im+"r83.png",'0px','0px']
                                            }]
                                        },
                                        {
                                            id: 'btn-v2',
                                            type: 'group',
                                            rect: ['617', '332', '262', '108', 'auto', 'auto'],
                                            title: 'Botón Verdadero',
                                            cursor: 'pointer',
                                            userClass: "btn",
                                            c: [
                                            {
                                                id: 'btn-v2-1',
                                                type: 'image',
                                                rect: ['0px', '0px', '262px', '108px', 'auto', 'auto'],
                                                fill: ["rgba(0,0,0,0)",im+"r87.png",'0px','0px']
                                            },
                                            {
                                                id: 'btn-v2-2',
                                                type: 'image',
                                                rect: ['0px', '0px', '260px', '107px', 'auto', 'auto'],
                                                fill: ["rgba(0,0,0,0)",im+"r82.png",'0px','0px']
                                            },
                                            {
                                                id: 'btn-v2-3',
                                                type: 'image',
                                                rect: ['0px', '0px', '260px', '107px', 'auto', 'auto'],
                                                fill: ["rgba(0,0,0,0)",im+"r84.png",'0px','0px']
                                            }]
                                        },
                                        {
                                            id: 'r98',
                                            type: 'image',
                                            rect: ['254px', '73px', '695px', '232px', 'auto', 'auto'],
                                            title: '¿La innovación puede adaptarse a diferentes contextos, siempre es posible modificar un proceso, procedimiento, valor, característicd, propiedad entre otros elementos para modificar el tradicional resultado?',
                                            fill: ["rgba(0,0,0,0)",im+"r98.png",'0px','0px']
                                        }]
                                    },
                                    {
                                        id: 'slide11',
                                        type: 'rect',
                                        rect: ['0px', '0px', '1022px', '413px', 'auto', 'auto'],
                                        fill: ["rgba(255,255,255,0.00)"],
                                        stroke: [0,"rgba(0,0,0,1)","none"],
                                        userClass: "sliders",
                                        c: [
                                        {
                                            id: 'r106',
                                            type: 'image',
                                            rect: ['603px', '-56px', '542px', '536px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"r106.png",'0px','0px']
                                        },
                                        {
                                            id: 'slider_text_10',
                                            type: 'rect',
                                            rect: ['30px', '-46px', '610px', '439px', 'auto', 'auto'],
                                            fill: ["rgba(255,255,255,0.00)"],
                                            stroke: [0,"rgb(0, 0, 0)","none"],
                                            userClass: "containers"
                                        }]
                                    },
                                    {
                                        id: 'slide12',
                                        type: 'rect',
                                        rect: ['0px', '0px', '1022px', '413px', 'auto', 'auto'],
                                        fill: ["rgba(255,255,255,0.00)"],
                                        stroke: [0,"rgba(0,0,0,1)","none"],
                                        userClass: "sliders",
                                        c: [
                                        {
                                            id: 'r108',
                                            type: 'image',
                                            rect: ['614px', '-117px', '441px', '613px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"r108.png",'0px','0px']
                                        },
                                        {
                                            id: 'slider_text_11',
                                            type: 'rect',
                                            rect: ['30px', '-46px', '610px', '439px', 'auto', 'auto'],
                                            fill: ["rgba(255,255,255,0.00)"],
                                            stroke: [0,"rgb(0, 0, 0)","none"],
                                            userClass: "containers"
                                        }]
                                    },
                                    {
                                        id: 'slide13',
                                        type: 'rect',
                                        rect: ['0px', '0px', '1022px', '413px', 'auto', 'auto'],
                                        fill: ["rgba(255,255,255,0.00)"],
                                        stroke: [0,"rgba(0,0,0,1)","none"],
                                        userClass: "sliders",
                                        c: [
                                        {
                                            id: 'r112',
                                            type: 'image',
                                            rect: ['501px', '-33px', '591px', '543px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"r112.png",'0px','0px']
                                        },
                                        {
                                            id: 'slider_text_12',
                                            type: 'rect',
                                            rect: ['30px', '-46px', '610px', '439px', 'auto', 'auto'],
                                            fill: ["rgba(255,255,255,0.00)"],
                                            stroke: [0,"rgb(0, 0, 0)","none"],
                                            userClass: "containers"
                                        }]
                                    }]
                                }]
                            },
                            {
                                id: 'btn-atr',
                                type: 'image',
                                rect: ['31px', '548px', '46px', '46px', 'auto', 'auto'],
                                title: 'botón atrás',
                                tabindex: '10',
                                fill: ["rgba(0,0,0,0)",im+"btn-atr.png",'0px','0px']
                            },
                            {
                                id: 'btn_sig',
                                type: 'image',
                                rect: ['92px', '548px', '47px', '46px', 'auto', 'auto'],
                                title: 'botón siguiente',
                                tabindex: '11',
                                fill: ["rgba(0,0,0,0)",im+"btn_sig.png",'0px','0px']
                            },
                            {
                                id: 'btn_inicio',
                                type: 'image',
                                rect: ['949px', '0px', '58px', '73px', 'auto', 'auto'],
                                title: 'botón inicio',
                                tabindex: '9',
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"r76.png",'0px','0px']
                            }]
                        },
                        {
                            id: 'stage4',
                            type: 'group',
                            rect: ['0px', '16', '1136', '806', 'auto', 'auto'],
                            c: [
                            {
                                id: 'Rectangle3',
                                type: 'rect',
                                rect: ['0px', '-16px', '1024px', '640px', 'auto', 'auto'],
                                fill: ["rgba(255,255,255,1.00)"],
                                stroke: [0,"rgb(0, 0, 0)","none"]
                            },
                            {
                                id: 'r69Copy2',
                                type: 'image',
                                rect: ['590px', '421px', '546px', '385px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"r69.png",'0px','0px']
                            },
                            {
                                id: 'r68Copy2',
                                type: 'image',
                                rect: ['0px', '10px', '393px', '51px', 'auto', 'auto'],
                                title: 'título: ¿qué son la creatividad y la innovación?',
                                tabindex: '3',
                                fill: ["rgba(0,0,0,0)",im+"r68.png",'0px','0px']
                            },
                            {
                                id: 'icon_bio',
                                type: 'image',
                                rect: ['614px', '72px', '566px', '564px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"icon_bio.svg",'0px','0px']
                            },
                            {
                                id: 'text_biografia',
                                type: 'text',
                                rect: ['67px', '115px', '591px', '372px', 'auto', 'auto'],
                                text: "<p style=\"margin: 0px;\">​</p>",
                                align: "left",
                                font: ['Arial, Helvetica, sans-serif', [24, "px"], "rgba(0,0,0,1)", "400", "none", "normal", "break-word", "normal"],
                                textStyle: ["", "", "", "", "none"]
                            },
                            {
                                id: 'btn_inicio2',
                                type: 'image',
                                rect: ['949px', '-16px', '58px', '73px', 'auto', 'auto'],
                                title: 'botón inicio',
                                tabindex: '9',
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"r76.png",'0px','0px']
                            }]
                        },
                        {
                            id: 'stage5',
                            type: 'group',
                            rect: ['0px', '16', '1136', '806', 'auto', 'auto'],
                            c: [
                            {
                                id: 'Rectangle3Copy',
                                type: 'rect',
                                rect: ['0px', '-16px', '1024px', '640px', 'auto', 'auto'],
                                fill: ["rgba(255,255,255,1.00)"],
                                stroke: [0,"rgb(0, 0, 0)","none"]
                            },
                            {
                                id: 'r69Copy3',
                                type: 'image',
                                rect: ['590px', '421px', '546px', '385px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"r69.png",'0px','0px']
                            },
                            {
                                id: 'r68Copy3',
                                type: 'image',
                                rect: ['0px', '10px', '393px', '51px', 'auto', 'auto'],
                                title: 'título: ¿qué son la creatividad y la innovación?',
                                tabindex: '3',
                                fill: ["rgba(0,0,0,0)",im+"r68.png",'0px','0px']
                            },
                            {
                                id: 'icon_bioCopy',
                                type: 'image',
                                rect: ['614px', '72px', '566px', '564px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"icon_bio.svg",'0px','0px']
                            },
                            {
                                id: 'text_creditos',
                                type: 'text',
                                rect: ['67px', '115px', '591px', '372px', 'auto', 'auto'],
                                text: "<p style=\"margin: 0px;\">​</p>",
                                align: "left",
                                font: ['Arial, Helvetica, sans-serif', [24, "px"], "rgba(0,0,0,1)", "400", "none", "normal", "break-word", "normal"],
                                textStyle: ["", "", "", "", "none"]
                            },
                            {
                                id: 'btn_inicio3',
                                type: 'image',
                                rect: ['949px', '-16px', '58px', '73px', 'auto', 'auto'],
                                title: 'botón inicio',
                                tabindex: '9',
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"r76.png",'0px','0px']
                            }]
                        },
                        {
                            id: 'preload',
                            type: 'group',
                            rect: ['0', '0', '1024', '640', 'auto', 'auto'],
                            c: [
                            {
                                id: 'fondo-preload',
                                type: 'rect',
                                rect: ['0px', '0px', '1024px', '640px', 'auto', 'auto'],
                                fill: ["rgba(255,255,255,1)"],
                                stroke: [0,"rgb(0, 0, 0)","none"]
                            },
                            {
                                id: 'logo',
                                type: 'image',
                                rect: ['462px', '270px', '100px', '100px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"logo.svg",'0px','0px']
                            }]
                        }
                    ],
                    style: {
                        '${Stage}': {
                            isStage: true,
                            rect: ['null', 'null', '1024px', '640px', 'auto', 'auto'],
                            overflow: 'hidden',
                            fill: ["rgba(255,255,255,1)"]
                        }
                    }
                },
                timeline: {
                    duration: 0,
                    autoPlay: true,
                    data: [

                    ]
                }
            }
        };

    AdobeEdge.registerCompositionDefn(compId, symbols, fonts, scripts, resources, opts);

    if (!window.edge_authoring_mode) AdobeEdge.getComposition(compId).load("index_edgeActions.js");
})("EDGE-247297");

var ST = "#Stage_";
var S = "Stage_";
ivo.info({
    title: "Guia General",
    autor: "Edilson Laverde Molina",
    date: "",
    email: "edilsonlaverde_182@hotmail.com",
    icon: 'https://www.ucundinamarca.edu.co/images/iconos/favicon_verde_1.png',
    facebook: "https://www.facebook.com/edilson.laverde.molina"
});
var audios = [{
    url: "sonidos/click.mp3",
    name: "clic"
    },{
    url: "sonidos/check.ogg",
    name: "good"
    },{
    url: "sonidos/explosion.ogg",
    name: "fail"
}];

var slider_text = [
    {
        id: ST+"slider_text_1",
        html: `
        <h1 class="title_udec">¿Qué es la creatividad?</h1>
        <ul class="items_udec">
            <li>Según el diccionario de la Real Academia Española, creatividad se traduce a la facultad de crear.</li>
            <li>Pero la creatividad va mucho más allá de esa facultad, podemos traducir que es la posibilidad de generar nuevas ideas o conceptos, sobre lo ya conocido para construir novedosas soluciones.</li>
        </ul>
        <p class="item_logo2">
            <img src="images/sonido.png" alt="creatividad"  width="60">
            <span class="item_text_logo2">Recuerde que al finalizar este recurso digital de aprendizaje, debe realizar la Actividad 1: Innovación y creatividad, aliados en la resolución de problemas</span>
        </p>
        
        `
    },{
        id: ST+"slider_text_2",
        html: `
        <h1 class="title_udec">Sinónimos</h1>
        <p class="items_udec_text">Para comprender más fácilmente qué es la creatividad y cuál es su importancia, podemos hacer alusión a los siguientes sinónimos:</p>
        <ul class="items_udec m-0">
            <li>Pensamiento original</li>
            <li>Pensamiento divergente</li>
            <li>Imaginación</li>
            <li>Ingenio</li>
            <li>Creación</li>
            <li>Invención</li>
        </ul>
        <a target="_blank" class="link_udec" href="./downloads/Caracteristicas_de_personas_creativas.pdf">
            <p class="item_logo">
                <img src="images/mano.png" alt="creatividad" width="60">
                <span class="item_text_logo">Haga clic para consultar el recurso: Características de personas innovadoras y creativas</span>
            </p>
        </a>
        `
    },{
        id: ST+"slider_text_4",
        html: `
        <h1 class="title_udec">Límites y esquemas</h1>
        <di class="colums_2">
            <p class="items_udec_text">Sin duda, pensar fuera de los límites que nos propone la sociedad puede ayudar a convertirnos en una persona altamente creativa, capaz de modificar cualquier esquema preestablecido y por supuesto modificar los cimientos como se hacen las cosas tradicionalmente. </p>
            <p class="items_udec_text">Los esquemas marcan una pauta, sobre  procedimientos, personas, procesos, productos y/o servicios. No obstante, no se debe caer en la implementación rigurosa de dichos esquemas, para no dar origen a los mismos resultados que se han obtenido por años, sin ningún tipo de valor agregado o factor diferencial.</p>
        </div>
        `
    },{
        id: ST+"slider_text_5",
        html: `
        <h1 class="title_udec">¿Pensamiento convencional?</h1>
        <di class="colums_2">
            <p class="items_udec_text">El pensamiento convencional genera un bloqueo creativo, pues de alguna manera encasilla el proceder, ciñendo toda actividad a realizarse de la misma forma que años atrás, sin personalizar ningún aspecto. Esto traerá los mismos resultados de siempre, sin novedad, sin diferenciación y por supuesto nada que logre captar la atención del contexto actual.</p>
            <p class="items_udec_text">Cuando se es pequeño y se pregunta ¿qué quiere ser cuando sea grande?, no hay ninguna limitación para la respuesta. En verdad cualquier opción es viable. Sin embargo, al crecer bajo preceptos marcados, esquemas, filosofías de vida y más, las posibilidades desaparecen, lo que era alcanzable ya no parece una idea sensata. Pero ¿en qué momento se deja de pensar cómo niño?, ¿en qué momento se pierde la imaginación?. Es fundamental identificar los limitantes para abolirlos.</p>
        </div>
        `
    },{
        id: ST+"slider_text_6",
        html: `
        <h1 class="title_udec">
            ¿La creatividad es necesaria?
        </h1>
        <div  style="margin-left:20px;" class="colums_2">
                <ul style="width:50%" class="items_udec">
                    <li>La respuesta es un rotundo SÍ.</li>
                    <li>Si no se es audaz en lo que se hace, no se tendrá ningún valor diferencial con respecto a los demás. Pero entonces, la pregunta sería: ¿Por qué es tan importante ser diferente?.</li>
                </ul>

                <ul style="width:50%" class="items_udec">
                    <li>Simple. En la actualidad cualquier proceso, desde postularse a un cargo, presentar una entrevista, ofrecer un producto o servicio o simplemente el desarrollo de actividades cotidianas, requieren un elemento que lo haga resaltar sobre los demás, con diferentes propósitos; por ejemplo, tener mayores utilidades, ganar el cargo al que se postuló, entre otros. </li>
                    <li>¿Cómo se logra esto?, con la CREATIVIDAD. Rompiendo esquemas, cambiando procesos, modificando variables, alterando estructuras definidas en aras de obtener resultados novedosos.</li>
                </ul>
            
        </div>
        `
    },{
        id: ST+"slider_text_7",
        html: `
        <h1 class="title_udec">Elementos para 
        despertar la creatividad</h1>
        <ul class="items_udec">
            <li>Es importante recordar que las nuevas ideas surgen de lo conocido, de lo existente, por lo tanto, todos pueden despertar su creatividad.</li>
            <li>La creatividad debe estimularse, ejercitarse para desarrollarla y aplicarla en cualquier contexto.</li>
            <li>Es importante ser consiente del nivel creativo actual de cada uno. Contar con un autodiagnóstico permite la implementación de medidas efectivas para despertar o mejorar la creatividad y aplicarla en la cotidianidad. </li>

        </ul>
        <a target="_blank" class="link_udec" href="./downloads/SCAMPER.pdf">
            <p class="item_logo items_udec_center">
                <img src="images/mano.png" alt="creatividad" width="60">
                <span class="item_text_logo">Haga clic para consultar el recurso: SCAMPER</span>
            </p>
        </a>
        <a target="_blank" class="link_udec" href="./downloads/Brainstorm.pdf">
            <p class="item_logo items_udec_center">
                <img src="images/mano.png" alt="creatividad" width="60">
                <span class="item_text_logo">Haga clic para ver el recurso: Brainstorm</span>
            </p>
        </a>
        <a target="_blank" class="link_udec" href="./downloads/PNI.pdf">
            <p class="item_logo items_udec_center">
                <img src="images/mano.png" alt="creatividad" width="60">
                <span class="item_text_logo">Haga clic para acceder al recurso: PNI</span>
            </p>
        </a>
        `
    },
    {
        id: ST+"slider_text_8",
        html: `
        <h1 class="title_udec">Innovación ¿Qué es?</h1>
        <ul class="items_udec">
            <li>La innovación alude a la capacidad de introducir nuevos elementos a lo ya existente para mejorarlo, o incluso crear nuevos conceptos.</li>
            <li>La innovación puede adaptarse a diferentes contextos, siempre es posible modificar un proceso, procedimiento, valor, característica, propiedad entre otros elementos para modificar el tradicional resultado.</li>
        </ul>
        <a target="_blank" class="link_udec" href="./downloads/Fases_de_la_Innovacion.pdf">
            <p class="item_logo">
                <img src="images/mano.png" alt="creatividad"  width="60">
                <span class="item_text_logo">Haga clic para consultar el recurso: Fases de la innovación</span>
            </p>
        </a>
        `
    },
    {
        id: ST+"slider_text_10",
        html: `
        <h1 class="title_udec">Importancia de la innovación</h1>
        <p class="items_udec_text">Dado que existen múltiples contextos donde se puede aplicar la innovación, conviene indicar que su implementación permite obtener relevantes beneficios, bien sea un individuo, una pequeña empresa o una gran corporación.</p>
        <p class="items_udec_text">Sea cual sea el resultado el proceso se concibe como una ganancia dado que dicha experiencia enriquece los procesos a desarrollar en el futuro. Por lo que no se debe «echar en saco roto» los posibles fracasos.</p>
        <a target="_blank" class="link_udec" href="./downloads/Casos_de_Exito_de_Innovacion.pdf">
            <p class="item_logo">
                <img src="images/mano.png" alt="creatividad"  width="60">
                <span class="item_text_logo">Haga clic para ver el recurso: Casos de éxito innovación</span>
            </p>
        </a>
        `
    },
    {
        id: ST+"slider_text_11",
        html: `
        <h1 class="title_udec">Conclusión</h1>
        <p class="items_udec_text">Se identifica un estrecho vínculo entre la creatividad y la innovación, ya que una persona creativa está en una búsqueda permanente de nuevos caminos y nuevas formas de realizar los procesos convencionales, y la innovación determina el factor diferencial de esos nuevos elementos, procesos, productos, servicios, etc.</p>
        <p class="items_udec_text">La innovación y la creatividad son características relevantes con las que debe contar una persona, pues las necesidades y el contexto así lo ameritan. El consumo ha cambiado, así como las costumbres, hábitos, las personas,  y para ser competitivos es fundamental adaptarse y responder a esas necesidades.</p>
        <a target="_blank" class="link_udec" href="./downloads/Design_Lean.pdf">
            <p class="item_logo">
                <img src="images/mano.png" alt="creatividad"  width="60">
                <span class="item_text_logo">Haga clic para consultar el recurso: Reconociendo design thinking y lean starUp</span>
            </p>
        </a>
        `
    },
    {
        id: ST+"slider_text_12",
        html: `
        <h1 class="title_udec">Ejercicios</h1>
        <p class="item_logo2">
            <img src="images/sonido.png" alt="creatividad"  width="60">
            <span class="item_text_logo2">Recuerde que al finalizar este recurso digital de aprendizaje, debe realizar la Actividad 2: Design Thinking y Lean StarUp, aliados para el trabajo creativo</span>
        </p>
        
        <a target="_blank" class="link_udec" href="./downloads/Ejercicios_creatividad.pdf">
            <p class="item_logo">
                <img src="images/mano.png" alt="creatividad"  width="60">
                <span class="item_text_logo">Haga clic para consultar el recurso: Ejercicios de creatividad</span>
            </p>
        </a>
        <a target="_blank" class="link_udec" href="./downloads/Ejercicios_innovacion.pdf">
            <p class="item_logo">
                <img src="images/mano.png" alt="creatividad"  width="60">
                <span class="item_text_logo">Haga clic para acceder al recurso: Ejercicios de innovación</span>
            </p>
        </a>
        `
    },
    {
        id: ST+"text_biografia",
        html: `
        <h1 style="color:#79C000;" class="title_udec">Bibliografía</h1>
        
        <p class="items_udec_text">Velásquez, Bertha. (2010). La creatividad como práctica para el desarrollo del cerebro total.  Recuperado de: http://www.scielo.org.co/pdf/tara/n13/n13a14.pdf</p>
        
       
        `
    },
    {
        id: ST+"text_creditos",
        html: `
        <div class="creditos">
        <p>
            Metodologías de Innovación -  Design Thinking y Lean StarUp
            <span>¿Qué son la creatividad y la innovación?</span>
        </p>
        <p>
            Líder de Proyectos
            <span>Viviana Jaramillo</span>
        </p>
        <p>
            Experto en contenido
            <span>Javier Hernando Ruiz Farfán</span>
        </p>
        <p>
            Diseñador Instruccional	
            <span>Amanda Pedraza</span>
            <span>Gabriela Strauss</span>
        </p>
        <p>
            Programación
            <span>Edilson Laverde Molina</span>
        </p>
        <p>
            Diseño
            <span>Andrés Martínez</span>
            <span>Nazly María Victoria Díaz Vera</span>
            <span>Sergio Sarmiento</span>
        </p>
        <p>
            Diseñador Multimedia	
            <span>Ginés Velásquez	</span>
            <span>Luis Francisco Sierra</span>
        </p>
        <p>
            Oficina de Educación Virtual y a Distancia<br>
            Universidad de Cundinamarca<br>
            2022
        <p>
    </div>`
    },
]

function main(sym) {
    var t = null;
    udec = ivo.structure({
        created: function () {
            t = this;
            //precarga audios//
            t.setText();
            t.events();
            t.animation();
            ivo.load_audio(audios, onComplete = function () {
                ivo(ST + "preload").hide();
                stage1.play();
            });
        },
        methods: {
            setText: function () {
                let limit = slider_text.length;
                for (let i = 0; i < limit; i++) {
                    ivo(slider_text[i].id).html(slider_text[i].html);
                }
            },

            events: function () {
                
                let slider=ivo(ST+"slider").slider({
                    slides:'.sliders',
                    btn_next:ST+"btn_sig",
                    btn_back:ST+"btn-atr",
                    rules:function(rule){
                        console.log("rules"+rule);
                    },
                    onMove:function(page){
                        ivo.play("clic");
                        console.log("onMove"+page);
                        if(page>=8){
                            ivo(ST+"btn_creatividad").addClass("gris");
                        }
                        if(page>=13){
                            ivo(ST+"btn_inovacion").addClass("gris");
                        }
                    },
                    onFinish:function(){

                    }
                });

                //eventos preguntas
                ivo(ST+"btn-v1").on("click",function(){
                    ivo.play("good");
                });
                ivo(ST+"btn-f1").on("click",function(){
                    ivo.play("fail");
                });
                ivo(ST+"btn-v2").on("click",function(){
                    ivo.play("good");
                });
                ivo(ST+"btn-f2").on("click",function(){
                    ivo.play("fail");
                });

                //ocultamos las primeras capas del boton de la clase .btn//
                //recorremos la clase con los diferentes botones//
                ivo(".btn").each(function (i, e) {
                    //obtenemos el id del boton//
                    let id = ivo(i).attr("id");
                    //ocultamos las capas 2 y 3//
                    console.log("#"+id + "-2");
                    ivo("#"+id + "-2").hide();
                    ivo("#"+id + "-3").hide();
                });

                //efecto hover para activar la segunda capa//
                ivo(".btn").on("click",function(){
                    //establecemos los inidices para resetear la opcion seleccionada//
                    let indices= ['Stage_btn-v',"Stage_btn-f"];
                    //obtenemos el id del boton//
                    let id = ivo(this).attr("id");
                    //recorremos los inidices los comparmos con el id del boton//
                    for(let i=0;i<indices.length;i++){
                        //analizamos si el botón contiene el indice//
                        if(id.indexOf(indices[i])!=-1){
                            //obtenemos el valor restante del id//
                            let index = id.replace(indices[i],"");
                            //ocultamos las capas 2 y 3//
                            ivo("#"+indices[0] + index + "-2").hide();
                            ivo("#"+indices[0] + index + "-3").hide();
                            ivo("#"+indices[1] + index + "-2").hide();
                            ivo("#"+indices[1] + index + "-3").hide();
                            ivo("#"+indices[i] + index + "-3").show();
                            //ivo.play("clic");
                            break;
                        }
                    }   

                })
                .on("mouseover", function () {
                    let id = ivo(this).attr("id");
                    ivo("#"+id + "-2").show();
                }).on("mouseout", function () {
                    let id = ivo(this).attr("id");
                    ivo("#"+id + "-2").hide();
                });
                
                var t = this;
                /**
                 * @description Evento de click y enter en el boton de inicio
                 */
                let startCurse=()=>{
                    stage1.timeScale(5).reverse();
                    stage2.timeScale(1).play();
                    ivo.play("clic");
                };

                ivo(ST+"btn_siguiente").on("keypress", function (e) {
                    if(e.which == 13) { startCurse();}
                })
                .on("click", function () {
                    startCurse();
                });
                /** 
                * @description Evento de click y enter de la apertura del topico creatividad
                */
                let openCreatividad=()=>{
                    stage2.timeScale(5).reverse();
                    stage3.timeScale(1).play();
                    ivo.play("clic");
                    slider.go(1);
                };

                ivo(ST+"btn_creatividad").on("keypress", function (e) {
                    if(e.which == 13) { openCreatividad();}
                })
                .on("click", function () {
                    openCreatividad();
                });
                /**
                * @description Evento de click y enter de la apertura del topico creatividad
                */
                let openInnovacion=()=>{
                    stage2.timeScale(5).reverse();
                    stage3.timeScale(1).play();
                    ivo.play("clic");
                    slider.go(9);
                };
                ivo(ST+"btn_inovacion").on("keypress", function (e) {
                    if(e.which == 13) { openInnovacion();}
                })
                .on("click", function () {
                    openInnovacion();
                });
                /**
                 * @description Evento de click y enter en el boton de inicio
                 */
                let openMenu=()=>{
                    stage3.timeScale(5).reverse();
                    stage2.timeScale(1).play();
                    ivo.play("clic");
                };
                ivo(ST+"btn_inicio").on("keypress", function (e) {
                    if(e.which == 13) { openMenu();}
                })
                .on("click", function () {
                    openMenu();
                });



                /**
                 * @description Evento de click y enter de la apertura de los creditos
                 */
                let openCreditos=()=>{
                    creditos.timeScale(1).play();
                    ivo.play("clic");
                }
                ivo(ST+"btn_creditos").on("keypress", function (e) {
                    if(e.which == 13) { openCreditos();}
                })
                .on("click", function () {
                    openCreditos();
                });

                let closeCreditos=()=>{
                    creditos.timeScale(5).reverse();
                    ivo.play("clic");
                }   
                ivo(ST+"btn_inicio3").on("keypress", function (e) {
                    if(e.which == 13) { closeCreditos();}
                })
                .on("click", function () {
                    closeCreditos();
                });

                /**
                 * @description Evento de click y enter de la apertura de la bibliografia
                 */
                let openBibliografia=()=>{
                    bibliografia.timeScale(1).play();
                    ivo.play("clic");
                }   
                ivo(ST+"btn_bibliografia").on("keypress", function (e) {
                    if(e.which == 13) { openBibliografia();}
                })
                .on("click", function () {
                    openBibliografia();
                });

                let closeBibliografia=()=>{
                    bibliografia.timeScale(5).reverse();
                    ivo.play("clic");
                }   
                ivo(ST+"btn_inicio2").on("keypress", function (e) {
                    if(e.which == 13) { closeBibliografia();}
                })
                .on("click", function () {
                    closeBibliografia();
                });

                //adicionamos clase a las imagenes que esten en el link//
                $(".link_udec img").addClass("animated pulse infinite");

                ivo(".link_udec").on("click", function () {
                    //otenemos la imagen dentro
                    let img = $(this).find("img");
                    //quitamos la clase de animacion
                    img.removeClass("animated pulse infinite");
                });

                //adicionamos clase a las imagenes que esten en el link//
                $(".link_udec img").addClass("animated pulse infinite");

                ivo(".link_udec").on("click", function () {
                    //otenemos la imagen dentro
                    let img = $(this).find("img");
                    //quitamos la clase de animacion
                    img.removeClass("animated pulse infinite");
                });


            },
            animation: function () {
                stage1 = new TimelineMax();
                stage1.append(TweenMax.from(ST + "stage1", .8, {x: 1300, opacity: 0}), 0);
                stage1.append(TweenMax.from(ST + "r63", .8, {x: 1300, opacity: 0}), 0);
                stage1.append(TweenMax.staggerFrom(".tuercas", .4, {x: 100, opacity: 0, scaleY: 6, ease: Elastic.easeOut.config(0.3, 0.4)}, .2), 0);
                stage1.append(TweenMax.from(ST + "r66", .8, {y: 1300, opacity: 0}), 0);
                stage1.append(TweenMax.from(ST + "btn_siguiente", .8, {scale: 0, opacity: 0, rotation:900}), 0);
                stage1.stop();

                stage2 = new TimelineMax();
                stage2.append(TweenMax.from(ST + "stage2", .8, {x: 1300, opacity: 0}), 0);
                stage2.append(TweenMax.from(ST + "r69", .8, {x: 1300, opacity: 0}), 0);
                stage2.append(TweenMax.staggerFrom(".btn_options", .4, {x: 100, opacity: 0, scaleY: 6, ease: Elastic.easeOut.config(0.3, 0.4)}, .2), 0);
                stage2.append(TweenMax.staggerFrom(".btn_temas", .4, {x: 100, opacity: 0, scaleY: 6, ease: Elastic.easeOut.config(0.3, 0.4)}, .2), 0);
                stage2.stop();

                stage3 = new TimelineMax();
                stage3.append(TweenMax.from(ST + "stage3", .8, {x: 1300, opacity: 0}), 0);
                stage3.append(TweenMax.from(ST + "slider", .8, {display: 'none'}), 0);
                stage3.stop();

                bibliografia = new TimelineMax();
                bibliografia.append(TweenMax.from(ST + "stage4", .8, {x: 1300, opacity: 0}), 0);
                bibliografia.stop();
                
                creditos = new TimelineMax();
                creditos.append(TweenMax.from(ST + "stage5", .8, {x: 1300, opacity: 0}), 0);
                creditos.stop();
            }
        }
    });
}